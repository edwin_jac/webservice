﻿using StreamJsonRpc;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PersonClient_WPF
{
    class VMPerson : INotifyPropertyChanged, IDataErrorInfo
    {
        private JsonRpc jsonRpc;

        // INotifyPropertyChanged Start
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private int _id;
        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                // for updating textblock defined in xaml
                NotifyPropertyChanged("Id");
            }
        }
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                // for updating textblock defined in xaml
                NotifyPropertyChanged("Name");
            }
        }
        // IDataErrorInfo Start
        private int _age;
        public int Age
        {
            get { return _age; }
            set
            {
                if (value <= 0)
                {
                    _errorMessage[0] = "Age must be positive";
                }
                else
                {
                    _errorMessage[0] = "";
                    _age = value;
                    NotifyPropertyChanged("Age");
                }
            }
        }
        // INotifyPropertyChanged End
        private string[] _errorMessage = new string[2] { string.Empty, string.Empty };
        public string Error
        {
            get { return null; }
        }
        public string this[string propertyName]
        {
            get
            {
                switch (propertyName)
                {
                    case "Name":
                        if (!String.IsNullOrEmpty(_errorMessage[0]))
                            return _errorMessage[0];
                        break;
                    case "Age":
                        if (!String.IsNullOrEmpty(_errorMessage[1]))
                            return _errorMessage[1];
                        break;
                }
                return string.Empty;
            }
        }
        // IDataErrorInfo End
        // View Model

        public ObservableCollection<Person> Persons { get; set; }
        private Person _selectedPerson;
        private bool isSelected = false;
        public Person SelectedPerson
        {
            get { return _selectedPerson; }
            set
            {
                _selectedPerson = value;
                if (value != null)
                {
                    Id = _selectedPerson.Id;
                    Name = _selectedPerson.Name;
                    Age = _selectedPerson.Age;
                    isSelected = true;
                }
            }
        }
        public ClientCommand AddCommand { get; set; }
        public ClientCommand UpdateCommand { get; set; }
        public ClientCommand DeleteCommand { get; set; }
        public VMPerson()
        {
            Persons = new ObservableCollection<Person>();
            AddCommand = new ClientCommand(this.OnAdd, this.CanAdd);
            UpdateCommand = new ClientCommand(this.OnUpdate, this.CanUpdateDelete);
            DeleteCommand = new ClientCommand(this.OnDelete, this.CanUpdateDelete);
            try
            {
                var stream = new NamedPipeClientStream(".", "StreamJsonRpcPersonPipe", PipeDirection.InOut, PipeOptions.Asynchronous);
                stream.ConnectAsync().Wait();
                jsonRpc = JsonRpc.Attach(stream);
            }
            catch (RemoteRpcException ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("RPC exception:\n" + ex.Message);
            }
            OnList();
        }
        public void OnList()
        {
            try
            {
                var persons = jsonRpc.InvokeAsync<List<Person>>("GetAllPeople").Result;
                Console.WriteLine($"Count of persons is {persons.Count}");
                Persons.Clear();
                foreach (var item in persons)
                {
                    Persons.Add(item);
                }
            }
            catch (RemoteRpcException ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("RPC exception:\n" + ex.Message);
            }
        }
        public bool CanAdd()
        {
            if (!String.IsNullOrEmpty(Name) && Age > 0)
                return true;
            else
                return false;
        }
        public void OnAdd()
        {
            if (!String.IsNullOrEmpty(_errorMessage[0]))
                return;
            try
            {
                Person p = new Person();
                p.Name = Name;
                p.Age = Age;
                var id = jsonRpc.InvokeAsync<int>("AddPerson", p).Result;
                OnList();
            }
            catch (RemoteRpcException ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("RPC exception:\n" + ex.Message);
            }
        }
        public bool CanUpdateDelete()
        {
            if (isSelected)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void OnUpdate()
        {
            if (!String.IsNullOrEmpty(_errorMessage[0]))
                return;
            try
            {
                Person p = new Person();
                p.Id = Id;
                p.Name = Name;
                p.Age = Age;
                jsonRpc.InvokeAsync("UpdatePerson", p).ConfigureAwait(true);
                OnList();
            }
            catch (RemoteRpcException ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("RPC exception:\n" + ex.Message);
            }
        }
        public void OnDelete()
        {
            if (!String.IsNullOrEmpty(_errorMessage[0]))
                return;
            try
            {
                jsonRpc.InvokeAsync("DeletePerson", Id).ConfigureAwait(true);
                OnList();
            }
            catch (RemoteRpcException ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("RPC exception:\n" + ex.Message);
            }
        }
    }
}