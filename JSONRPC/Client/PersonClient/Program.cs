﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using StreamJsonRpc;
using Newtonsoft.Json;

namespace PersonClient
{
    class Program
    {
        static async Task Main()
        {
            Console.WriteLine("Connecting to server...");
            using (var stream = new NamedPipeClientStream(".", "StreamJsonRpcPersonPipe", PipeDirection.InOut, PipeOptions.Asynchronous))
            {
                try
                {
                    await stream.ConnectAsync();
                    Console.WriteLine("Connected. Sending request...");
                    var jsonRpc = JsonRpc.Attach(stream);

                    /*
                    //testing for exception
                    var ps = await jsonRpc.InvokeAsync<Person>("GetPerson", 0);
                    Console.WriteLine($"Search Person: #0, {ps.Name}, {ps.Age}");
                    */

                    // test call - passing and returning an object
                    var pa = new Person() { Name = "Sherly", Age = 32 };
                    var id = await jsonRpc.InvokeAsync<int>("AddPerson", pa);
                    Console.WriteLine($"The ID of new person is {id}");
                    // test call - passing and returning an object
                    var pn = new Person() { Id = id, Name = "Sherly W.", Age = 57 };
                    await jsonRpc.InvokeAsync("UpdatePerson", pn);
                    var pu = await jsonRpc.InvokeAsync<Person>("GetPerson", id);
                    Console.WriteLine($"Updated Person: {pu.Id}, {pu.Name}, {pu.Age}");
                    await jsonRpc.InvokeAsync("DeletePerson", id);
                    Console.WriteLine($"The Person #{id} is deleted");
                    // test call - passing and returning an object
                    var persons = await jsonRpc.InvokeAsync<List<Person>>("GetAllPeople");
                    Console.WriteLine($"Count of persons is {persons.Count}");
                    foreach (var item in persons)
                    {
                        Console.WriteLine($"Person #{item.Id}: Name {item.Name}, Age {item.Age}");
                    }
                }catch(RemoteRpcException ex)
                {
                    //var errData = ex.ErrorData;
                    Console.WriteLine($">> Error:  {ex.Message} <<");
                }
                finally
                {
                    Console.WriteLine("Press any key");
                    Console.ReadKey();
                    Console.WriteLine("Terminating stream...");
                }
                
            }
        }
    }
}
