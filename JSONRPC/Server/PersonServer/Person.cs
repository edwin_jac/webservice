﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PersonServer
{
    [Table("Persons")]
    public class Person
    {
        private string _name;
        private int _age;

        [Required]
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [Required]
        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }
    }
}
