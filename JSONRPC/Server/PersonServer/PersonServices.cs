﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonServer
{
    class PersonServices
    {
        private readonly PersonDbContext db = new PersonDbContext();

        public PersonServices()
        {
            db.Database.CreateIfNotExists();
        }

        public List<Person> GetAllPeople()
        {
            List<Person> persons = new List<Person>();
            foreach (var p in db.Persons)
            {
                persons.Add(p);
            }
            return persons;
        }

        public Person GetPerson(int id)
        {
            if (id <= 0) throw new ArgumentException("Id shall be positive!");
            Person record;
            record = db.Persons.Where<Person>(p => p.Id == id).FirstOrDefault();

            return record;
        }

        public int AddPerson(Person person)
        {
            var item = db.Persons.Add(person);
            db.SaveChanges();
            //var record = db.Persons.Where<Person>(p => p.Id == item.Id).FirstOrDefault();
            return item.Id;
        }

        public void UpdatePerson(Person person)
        {
            var record = db.Persons.Where<Person>(p => p.Id == person.Id).FirstOrDefault();
            record.Name = person.Name;
            record.Age = person.Age;
            db.SaveChanges();
        }

        public void DeletePerson(int id)
        {
            var record = db.Persons.Find(id);
            db.Persons.Remove(record);
            db.SaveChanges();
        }

    }
}
