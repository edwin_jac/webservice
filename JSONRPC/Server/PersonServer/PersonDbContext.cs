using System;
using System.Data.Entity;
using System.Linq;
using System.IO;

namespace PersonServer
{
    public class PersonDbContext : DbContext
    {
        const string DbName = "person_db.mdf";
        static string DbPath = Path.Combine(Environment.CurrentDirectory, DbName);
        public PersonDbContext() : base($@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30") { }

        public virtual DbSet<Person> Persons { get; set; }
    }
}