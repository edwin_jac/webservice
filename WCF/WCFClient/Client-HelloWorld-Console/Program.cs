﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace Client_HelloWorld
{
    class Program
    {
        static void InvokeHello()
        {
            HelloServiceReference.Service1Client client = new HelloServiceReference.Service1Client();
            String result = client.GetData(689);
            Console.WriteLine(result);
            Console.ReadKey();
        }

        static void InvokeFibonacci()
        {
            FibServiceReference.IFibonacciService client = new FibServiceReference.FibonacciServiceClient();
            try
            {
                long FibNumber = client.GetNthFibonacci(-1);
                Console.WriteLine(FibNumber);
            }
            catch (FaultException<InvalidOperationException> ex)
            {
                InvalidOperationException actualEx = ex.Detail; // unwrap the actual exception
                Console.WriteLine("API Error: " + actualEx.Message);
            }
            catch (FaultException ex)
            {
                Console.WriteLine("API Error:" + ex.Message);
            }
            catch (System.ServiceModel.EndpointNotFoundException ex)
            {
                Console.WriteLine("Connection Error: " + ex.Message);
            }
            
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            InvokeHello();
            InvokeFibonacci();
        }
    }
}
