﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Client_HelloWorld.FibServiceReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="FibServiceReference.IFibonacciService")]
    public interface IFibonacciService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFibonacciService/GetNthFibonacci", ReplyAction="http://tempuri.org/IFibonacciService/GetNthFibonacciResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.InvalidOperationException), Action="http://tempuri.org/IFibonacciService/GetNthFibonacciInvalidOperationExceptionFaul" +
            "t", Name="InvalidOperationException", Namespace="http://schemas.datacontract.org/2004/07/System")]
        long GetNthFibonacci(int n);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFibonacciService/GetNthFibonacci", ReplyAction="http://tempuri.org/IFibonacciService/GetNthFibonacciResponse")]
        System.Threading.Tasks.Task<long> GetNthFibonacciAsync(int n);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IFibonacciServiceChannel : Client_HelloWorld.FibServiceReference.IFibonacciService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class FibonacciServiceClient : System.ServiceModel.ClientBase<Client_HelloWorld.FibServiceReference.IFibonacciService>, Client_HelloWorld.FibServiceReference.IFibonacciService {
        
        public FibonacciServiceClient() {
        }
        
        public FibonacciServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public FibonacciServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public FibonacciServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public FibonacciServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public long GetNthFibonacci(int n) {
            return base.Channel.GetNthFibonacci(n);
        }
        
        public System.Threading.Tasks.Task<long> GetNthFibonacciAsync(int n) {
            return base.Channel.GetNthFibonacciAsync(n);
        }
    }
}
