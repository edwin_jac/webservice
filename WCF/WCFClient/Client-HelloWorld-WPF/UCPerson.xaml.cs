﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client_HelloWorld_WPF_Framework
{
    /// <summary>
    /// Interaction logic for UCPerson.xaml
    /// </summary>
    public partial class UCPerson : UserControl
    {
        private VMPerson _vmPerson;
        public UCPerson()
        {
            InitializeComponent();
            _vmPerson = new VMPerson();
            this.DataContext = _vmPerson;
        }
    }
}
