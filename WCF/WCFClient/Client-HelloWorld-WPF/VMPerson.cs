﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Client_HelloWorld_WPF_Framework
{
    class VMPerson : INotifyPropertyChanged, IDataErrorInfo
    {
        // INotifyPropertyChanged Start
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private int _id;
        public int Id 
        {
            get { return _id; }
            set
            {
                _id = value;
                // for updating textblock defined in xaml
                NotifyPropertyChanged("Id");
            }
        }
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                // for updating textblock defined in xaml
                NotifyPropertyChanged("Name");
            }
        }
        // IDataErrorInfo Start
        private int _age;
        public int Age
        {
            get { return _age; }
            set
            {
                if (value <= 0)
                {
                    _errorMessage[0] = "Age must be positive";
                }
                else
                {
                    _errorMessage[0] = "";
                    _age = value;
                    NotifyPropertyChanged("Age");
                }
            }
        }
        // INotifyPropertyChanged End
        private string[] _errorMessage = new string[2] { string.Empty, string.Empty };
        public string Error
        {
            get { return null; }
        }
        public string this[string propertyName]
        {
            get
            {
                switch (propertyName)
                {
                    case "Name":
                        if (!String.IsNullOrEmpty(_errorMessage[0]))
                            return _errorMessage[0];
                        break;
                    case "Age":
                        if (!String.IsNullOrEmpty(_errorMessage[1]))
                            return _errorMessage[1];
                        break;
                }
                return string.Empty;
            }
        }
        // IDataErrorInfo End
        // View Model
        private PersonServiceReference.IPeopleService client;
        public ObservableCollection<PersonServiceReference.Person> Persons { get; set; }
        private PersonServiceReference.Person _selectedPerson;
        private bool isSelected = false;
        public PersonServiceReference.Person SelectedPerson 
        {
            get { return _selectedPerson; }
            set
            {
                _selectedPerson = value;
                if (value != null)
                {
                    Id = _selectedPerson.Id;
                    Name = _selectedPerson.Name;
                    Age = _selectedPerson.Age;
                    isSelected = true;
                }
            }
        }
        public ClientCommand AddCommand { get; set; }
        public ClientCommand UpdateCommand { get; set; }
        public ClientCommand DeleteCommand { get; set; }
        public VMPerson()
        {
            client = new PersonServiceReference.PeopleServiceClient();
            Persons = new ObservableCollection<PersonServiceReference.Person>();
            AddCommand = new ClientCommand(this.OnAdd, this.CanAdd);
            UpdateCommand = new ClientCommand(this.OnUpdate, this.CanUpdateDelete);
            DeleteCommand = new ClientCommand(this.OnDelete, this.CanUpdateDelete);
            OnList();
        }
        public void OnList()
        {
            if (!String.IsNullOrEmpty(_errorMessage[0]))
                return;
            try
            {
                var result = client.GetAllPeople();
                Persons.Clear();
                foreach(var p in result)
                {
                    Persons.Add(p);
                }
            }
            catch (FaultException<InvalidDataContractException> ex)
            {
                InvalidDataContractException actualEx = ex.Detail; // unwrap the actual exception
                MessageBox.Show("API Error: " + actualEx.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (FaultException ex)
            {
                MessageBox.Show("API Error:" + ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (System.ServiceModel.EndpointNotFoundException ex)
            {
                MessageBox.Show("Connection Error: " + ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        public bool CanAdd()
        {
            if(!String.IsNullOrEmpty(Name) && Age>0)
                return true;
            else
                return false;
        }
        public void OnAdd()
        {
            if (!String.IsNullOrEmpty(_errorMessage[0]))
                return;
            try
            {
                PersonServiceReference.Person p = new PersonServiceReference.Person();
                p.Name = Name;
                p.Age = Age;
                client.AddPerson(p);
                OnList();
            }
            catch (FaultException<InvalidDataContractException> ex)
            {
                InvalidDataContractException actualEx = ex.Detail; // unwrap the actual exception
                MessageBox.Show("API Error: " + actualEx.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (FaultException ex)
            {
                MessageBox.Show("API Error:" + ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (System.ServiceModel.EndpointNotFoundException ex)
            {
                MessageBox.Show("Connection Error: " + ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        public bool CanUpdateDelete()
        {
            if (isSelected)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void OnUpdate()
        {
            if (!String.IsNullOrEmpty(_errorMessage[0]))
                return;
            try
            {
                PersonServiceReference.Person p = new PersonServiceReference.Person();
                p.Id = Id;
                p.Name = Name;
                p.Age = Age;
                client.UpdatePerson(p);
                OnList();
            }
            catch (FaultException<InvalidDataContractException> ex)
            {
                InvalidDataContractException actualEx = ex.Detail; // unwrap the actual exception
                MessageBox.Show("API Error: " + actualEx.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (FaultException ex)
            {
                MessageBox.Show("API Error:" + ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (System.ServiceModel.EndpointNotFoundException ex)
            {
                MessageBox.Show("Connection Error: " + ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        public void OnDelete()
        {
            if (!String.IsNullOrEmpty(_errorMessage[0]))
                return;
            try
            {
                client.DeletePerson(Id);
                OnList();
            }
            catch (FaultException<InvalidDataContractException> ex)
            {
                InvalidDataContractException actualEx = ex.Detail; // unwrap the actual exception
                MessageBox.Show("API Error: " + actualEx.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (FaultException ex)
            {
                MessageBox.Show("API Error:" + ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (System.ServiceModel.EndpointNotFoundException ex)
            {
                MessageBox.Show("Connection Error: " + ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
