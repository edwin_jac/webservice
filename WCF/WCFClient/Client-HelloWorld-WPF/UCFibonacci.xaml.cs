﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client_HelloWorld_WPF_Framework
{
    /// <summary>
    /// Interaction logic for UCFibonacci.xaml
    /// </summary>
    public partial class UCFibonacci : UserControl
    {
        private VMFibonacci _vmFibonacci;
        public UCFibonacci()
        {
            InitializeComponent();
            _vmFibonacci = new VMFibonacci();
            this.DataContext = _vmFibonacci;
        }
    }
}
