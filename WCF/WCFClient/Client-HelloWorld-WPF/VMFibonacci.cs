﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Client_HelloWorld_WPF_Framework
{
    class VMFibonacci : INotifyPropertyChanged, IDataErrorInfo
    {
        private FibServiceReference.IFibonacciService client;
        public ClientCommand LoadCommand { get; set; }

        // INotifyPropertyChanged Start
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private long _fibNumber;
        public long FibNumber 
        {
            get { return _fibNumber; }
            set
            {
                _fibNumber = value;
                // for updating textblock defined in xaml
                NotifyPropertyChanged("FibNumber");
            }
        }
        // IDataErrorInfo Start
        private int _orderNumber;
        public int OrderNumber
        {
            get { return _orderNumber; }
            set
            {
                if (value <= 0)
                {
                    _errorMessage[0] = "N must be positive";
                }
                else
                {
                    _errorMessage[0] = "";
                    _orderNumber = value;
                    NotifyPropertyChanged("OrderNumber");
                }
            }
        }
        // INotifyPropertyChanged End
        private string[] _errorMessage = new string[1] { string.Empty};
        public string Error
        {
            get { return null; }
        }
        public string this[string propertyName]
        {
            get
            {
                switch (propertyName)
                {
                    case "OrderNumber":
                        if (!String.IsNullOrEmpty(_errorMessage[0]))
                            return _errorMessage[0];
                        break;
                }
                return string.Empty;
            }
        }
        // IDataErrorInfo End
        public VMFibonacci()
        {
            client = new FibServiceReference.FibonacciServiceClient();
            LoadCommand = new ClientCommand(this.OnLoad, this.CanLoad);
            OrderNumber = 1;
            FibNumber = 0;
        }
        public bool CanLoad()
        {
            // This check must be with UpdateSourceTrigger=PropertyChanged in xaml
            // otherwise, there might be problem to trigger erromessage clearance
            if (String.IsNullOrEmpty(_errorMessage[0]))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void OnLoad()
        {
            if (!String.IsNullOrEmpty(_errorMessage[0]))
                return;
            try
            {
                FibNumber = client.GetNthFibonacci(OrderNumber);
            }
            catch (FaultException<InvalidOperationException> ex)
            {
                InvalidOperationException actualEx = ex.Detail; // unwrap the actual exception
                MessageBox.Show("API Error: " + actualEx.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (FaultException ex)
            {
                MessageBox.Show("API Error:" + ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (System.ServiceModel.EndpointNotFoundException ex)
            {
                MessageBox.Show("Connection Error: "+ex.Message, "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
    }
}
