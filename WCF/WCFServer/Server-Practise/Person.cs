﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Server_Practise
{
    [DataContract]
    [Table("Persons")]
    public class Person
    {
        private string _name;
        private int _age;

        [Required]
        [Key]
        [DataMember]
        public int Id { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Name must be 50 characters or less"), MinLength(1, ErrorMessage = "Name must be at least 1 character")]
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [Required]
        [DataMember]
        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }
    }
}