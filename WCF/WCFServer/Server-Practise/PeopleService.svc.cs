﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Server_Practise
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PeopleService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PeopleService.svc or PeopleService.svc.cs at the Solution Explorer and start debugging.
    public class PeopleService : IPeopleService
    {
        private PersonModel db = new PersonModel();

        public PeopleService()
        {
            db.Database.CreateIfNotExists();
        }
        public List<Person> GetAllPeople() 
        {
            List<Person> persons = new List<Person>();
            foreach(var p in db.Persons)
            {
                persons.Add(p);
            }
            return persons;
        }

        public Person GetPerson(int id)
        {
            Person record;
            record = db.Persons.Where<Person>(p => p.Id == id).FirstOrDefault();

            return record;
        }

        public int AddPerson(Person person)
        {
            var item = db.Persons.Add(person);
            db.SaveChanges();
            //var record = db.Persons.Where<Person>(p => p.Id == item.Id).FirstOrDefault();
            return item.Id;
        }

        public void UpdatePerson(Person person)
        {
            var record = db.Persons.Where<Person>(p => p.Id == person.Id).FirstOrDefault();
            record.Name = person.Name;
            record.Age = person.Age;
            db.SaveChanges();
        }

        public void DeletePerson(int id)
        {
            var record = db.Persons.Find(id);
            db.Persons.Remove(record);
            db.SaveChanges();
        }

    }
}
