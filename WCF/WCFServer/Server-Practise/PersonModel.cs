using System;
using System.Data.Entity;
using System.Linq;

namespace Server_Practise
{
    public class PersonModel : DbContext
    {
        public PersonModel()
            : base("name=PersonSqlServer")//PersonMySQL//PersonLocalDB//PersonSqlServer
        {
        }

        public virtual DbSet<Person> Persons { get; set; }
    }

}