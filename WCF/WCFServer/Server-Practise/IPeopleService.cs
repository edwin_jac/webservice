﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Server_Practise
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPeopleService" in both code and config file together.
    [ServiceContract]
    public interface IPeopleService
    {
        [OperationContract]
        List<Person> GetAllPeople();
        [OperationContract]
        [FaultContract(typeof(InvalidDataContractException))]
        Person GetPerson(int id); 
        [OperationContract]
        [FaultContract(typeof(InvalidDataContractException))]
        int AddPerson(Person person);
        [OperationContract]
        [FaultContract(typeof(InvalidDataContractException))]
        void UpdatePerson(Person person);
        [OperationContract]
        [FaultContract(typeof(InvalidDataContractException))]
        void DeletePerson(int id);
    }
}
