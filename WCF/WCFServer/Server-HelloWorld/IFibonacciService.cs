﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Server_HelloWorld
{
    
    [ServiceContract]
    public interface IFibonacciService
    {
        [FaultContract(typeof(InvalidOperationException))] // allows us to throw IOE back to client
        [OperationContract]
        long GetNthFibonacci(int n);
    }


}
