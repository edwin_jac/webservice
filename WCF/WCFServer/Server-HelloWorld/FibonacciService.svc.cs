﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Server_HelloWorld
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "FibonacciService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select FibonacciService.svc or FibonacciService.svc.cs at the Solution Explorer and start debugging.
    public class FibonacciService : IFibonacciService
    {
        long getFib(int n)
        {
            if (n <= 1)
                return n;
            else
                return getFib(n - 1) + getFib(n - 2);
        }
        public long GetNthFibonacci(int n)//index from 1 => 1st
        {
            if (n <= 0)
            {
                throw new FaultException<InvalidOperationException>(new InvalidOperationException("N must be positive"));
            }
            return getFib(n-1);//index from 0 => 1st
        }
    }
}
